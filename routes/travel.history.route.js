var express = require('express');
var router = express.Router();
var db = require('../mongodb');
var travel = require('../model/travel.model');
var mongoose = require('mongoose');
mongoose.set('debug', true);


router.post("/find/travel", function(request, response) {

    var nationality = request.body.nationality;
    var passportno = request.body.passportNo;

    console.log('nationality :'+ nationality);
    console.log('passportno :'+ passportno);

    mongoose.model('Travel').find({nationality:nationality,passportno:passportno})
    .sort('-traveldate').exec(function(error, traveldetails){
        if(error){
            console.log(error);
            var responseObjWithTravelError = { 
                'code' : 500,
                'status' : 'Error Occured while fechting Travel Details.'
              }
              response.status(500).json(responseObjWithTravelError);    
              response.end;    
        }else{
            if(traveldetails.length > 0){
                console.log('Travel Details Count : '+ traveldetails.length);
                var responseObjWithTravelDetails = { 
                    'code' : 200,
                    'status' : 'Travel Details Fetched Sucessfully.',
                    'traveldetails' : traveldetails
                  }
                  response.status(200).json(responseObjWithTravelDetails);    
                  response.end;    
            }else{
                console.log('No record found for this Profile .. ');
                var responseObjWithNoTravelDetails = { 
                    'code' : 205,
                    'status' : 'No Travel Found for this particular Profile'
                  }
                  response.status(200).json(responseObjWithNoTravelDetails);    
                  response.end;    
 

                }
            }
        }
    );
  }
);

module.exports = router;