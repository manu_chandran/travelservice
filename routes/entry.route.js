var express = require('express');
var router = express.Router();
var db = require('../mongodb');
var travel = require('../model/travel.model');
var permit = require('../model/permit.model');
var mongoose = require('mongoose');
mongoose.set('debug', true);



router.post("/mark/entry", function(request, response) {

    var passportNo = request.body.passportNo;
    var nationality = request.body.nationality; 
    var fromCountry = request.body.fromCountry; 
    var flightNo = request.body.flightNo; 
    var travelTime =  request.body.travelDate; 
    var carierType = 'Airport'; 
    var visaType = 'On Arrival';
    var permitStatus =  'Used';
    var establishment = '4529923';
    var travelType = 'Entry';
    var permitCreationDate = new Date();
    var permitExpiryDate = permitCreationDate.setDate(permitCreationDate.getDate() + 30); 

    console.log('passportNo :'+ passportNo);
    console.log('nationality :'+ nationality);

    mongoose.model('Profile').count({passportno:passportNo,nationality:nationality},function(error, count){
        if(error){
            console.log(error);
            var responseObjWithProfileCountError = { 
                'code' : 500,
                'status' : 'Error Occured while fetching profile detais.'
          } 
          response.status(500).json(responseObjWithProfileCountError);    
          response.end;   
        }else{
            console.log('The Count of Profile with the passportNo :'+ passportNo + ' and nationality :'+nationality+' : '+count);
            if(count == 0){
                console.log('No Profile Exist to Mark the Travel......');
                var responseObjWithNoProfile = { 
                    'code' : 204,
                    'status' : 'No Profile Exist to Mark the Travel.'
              } 
              response.status(500).json(responseObjWithNoProfile);    
              response.end;   
            }else{
                         
                mongoose.model('Permit').count({passportno:passportNo,nationality:nationality,permitstatus:permitStatus},function(error, count){
                    if(error){
                        console.log(error);
                        var responseObjWithPermitCountError = { 
                            'code' : 500,
                            'status' : 'Error Occured while fechting Permit Count.'
                      } 
                      response.status(500).json(responseObjWithPermitCountError);    
                      response.end;   
                    }else{
                        if(count > 0){
                            console.log('Last Travel was Entry.......');
                            var responseObjWithLastTravelEntry = { 
                                'code' : 205,
                                'status' : 'Last Travel was Entry'
                          } 
                          response.status(200).json(responseObjWithLastTravelEntry);    
                          response.end;                            
                        }else{

                            var pNumber = Math.trunc((Math.random() * 10000000));
                            var permitNo = 'OA'+pNumber;            
                            mongoose.model('Permit').create({
                                permitno : permitNo,
                                visatype : visaType,
                                permitstatus : permitStatus,
                                createddate : permitCreationDate,
                                expirydate : permitExpiryDate,
                                establishment : establishment,
                                passportno : passportNo,
                                nationality : nationality
                            }, function (err, permit) {
                                  if (err) {
                                    console.log('Error Occured '+err);
                                    var responseObjWithPermitError = { 
                                          'code' : 500,
                                          'status' : 'Error Occured while creating Permit Details.'
                                    } 
                                    response.status(500).json(responseObjWithPermitError);    
                                    response.end;   
                                  }
                                  else {
                                    console.log('Created Permit'+permit);
                                    console.log('Goin to mark travel.....');                        
                                    mongoose.model('Travel').create({
                                        passportno : passportNo,
                                        nationality : nationality,
                                        traveltype : travelType,
                                        traveldate : travelTime,
                                        fromcountry : fromCountry,
                                        cariertype : carierType,
                                        flightno : flightNo,
                                        permitno : permitNo
                                    }, function (err, travel) {
                                          if (err) {
                                          console.log('Error Occured '+err);
                                          var responseObjWithTravelError = { 
                                                'code' : 500,
                                                'status' : 'Error Occured while fechting Permit Details.'
                                          } 
                                          response.status(500).json(responseObjWithTravelError);    
                                          response.end;    
                                          }
                                          else {
                                            console.log('Travel Mark'+travel);
                                            console.log('Travel Mark Successfuly for Entry');
                                            var responseObjWithTravelMarked = { 
                                                'code' : 200,
                                                'status' : 'Trave Marked Successfully...',
                                                'permitnumber' : permitNo
                                            } 
                                            response.status(200).json(responseObjWithTravelMarked);    
                                            response.end;
                                          }
                                       });            
                                  }
                               });
                        }
                    }
                });
            }
        }
    }
    );



}
);



module.exports = router;