var express = require('express');
var router = express.Router();
var db = require('../mongodb');
var user = require('../model/login.model');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('../config');
mongoose.set('debug', true);
  

router.post("/create/user", function(request, response) {

    var firstName = request.body.firstName;
    var middleName =  request.body.middleName;
    var lastName  = request.body.lastName;
    var createdDate = request.body.createdDate;
    var modifiedDate = request.body.modifiedDate;
    var userName = request.body.userName;
    var password = request.body.password;
    var isActive = request.body.isactive; 

    console.log('username :'+ userName);
    console.log('password :'+ password);
    console.log('createdDate :'+ createdDate);
    console.log('lastName :'+ lastName);
    console.log('isActive :'+ isActive);


    mongoose.model('User').count({username:userName},function(error, count){
        if(error){
            console.log(error);
            response.end('Error Occured while creating Record......');
        }else{
            console.log('The Count of Users with the same userID :'+count);
            if(count > 0){
                response.end('User ID Already exists......');
            }else{

                mongoose.model('User').create({
                    firstname : firstName,
                    middlename : middleName,
                    lastname : lastName,
                    createddate : createdDate,
                    modifieddate : modifiedDate,
                    username : userName,
                    password : password,
                    is_active : isActive
                }, function (err, user) {
                      if (err) {
                       response.send("There was a problem adding the information to the database.");
                          console.log('Error Occured '+err);
                      }
                      else {
                        console.log('Created User'+user);
                         response.send("Data Populated successfully");
                      }
                   });

            }

        }
    }
    );



}
);


router.post("/update/user", function(request, response) {
        var firstName = request.body.firstName;
        var middleName =  request.body.middleName;
        var lastName  = request.body.lastName;
        var isactive =  request.body.isactive;
        console.log('document ID :'+ request.body.id);

        var attributesToUpdate =  { 
            firstname :  firstName,
            middlename : middleName,
            lastname : lastName,
            is_active : isactive
         };
        console.log('attributesToUpdate :'+ attributesToUpdate);

    
        mongoose.model('User').findOneAndUpdate(
            {_id : new mongoose.Types.ObjectId(request.body.id)} ,{$set:attributesToUpdate}, {upsert: true, 'new': true}, function (err, user) {
              if (err) {
                console.log('Error Occured '+err);
                response.end("There was a problem adding the information to the database.");
              } else {
                if(user){
                    console.log('The User :'+user);
                    response.end("Data updated successfully.........");
                }
                response.end("No records Found for update.....");
              }
           });
    
    }
);



router.post("/delete/user", function(request, response) {
 
    var id = new mongoose.Types.ObjectId(request.body.id);
    var firstName = request.body.firstName;
    var middleName =  request.body.middleName;
    var lastName  = request.body.lastName;
    var isactive =  request.body.isactive;
    console.log('document ID :'+ id);
    mongoose.model('User').findOneAndRemove(
        {_id : new mongoose.Types.ObjectId(request.body.id)} , function (err, user) {
          if (err) {
            console.log('Error Occured '+err);
            response.end("There was a problem adding the information to the database.");
          } else {
            if(user){
                console.log(user);
                response.end("Data deleted successfully.........");
            }
            response.end("No records Found for update.....");
          }
       });

    }
);

/*    
router.post("/find/user", function(request, response) {
    
        var userName = request.body.userName;
        var password = request.body.password;
        console.log('username :'+ userName);
        console.log('password :'+ password);
    
        mongoose.model('User').findOne({
            username : userName
        }, function (err, user) {
                if (err) {
                    var errorResponse = { _code:999,_status:'Error Occured while Processing Request.'};
                    console.log('Error Occured '+err);
                    response.end(JSON.stringify(failedResponse));
                }
                else {
                  if(user){                      
                      console.log('UserName'+user.username);
                      let passwordFromDB = user.password
                      if(password == passwordFromDB){
                        const payload = {
                            user: user.username 
                          };
                        var token = jwt.sign(payload, config.secret, {
                        expiresIn : 1440 // expires in 24 hours
                        });

                        var successResponse = {code:200, status:'Success',token : token }  ;
                        response.end(JSON.stringify(successResponse));
                      }
                  }
                  var failedResponse = { code:900,status:'Login Failed,Invalid userName/password.'};
                  response.end(JSON.stringify(failedResponse));
                }
            });
    
    }
);
*/  

router.post("/find/user", function(request, response) {
    
        var userName = request.body.userName;
        var password = request.body.password;
        console.log('username :'+ userName);
        console.log('password :'+ password);
        
        if(userName == 'admin' && password == 'admin'){                      
            const payload = {
                user: userName 
                };
            var token = jwt.sign(payload, config.secret, {
            expiresIn : 1440 // expires in 24 hours
            });

            var successResponse = {code:200, status:'Success',token : token }  ;
            response.end(JSON.stringify(successResponse));
        }
        var failedResponse = { code:900,status:'Login Failed,Invalid userName/password.'};
        response.end(JSON.stringify(failedResponse));
    
    }
);

module.exports = router;