var express = require('express');
var router = express.Router();
var db = require('../mongodb');
var profile = require('../model/profile.model');
var mongoose = require('mongoose');
var travel = require('../model/travel.model');
mongoose.set('debug', true);


router.post("/create/profile", function(request, response) {

    var firstName = request.body.firstName;
    var middleName =  request.body.middleName;
    var lastName  = request.body.lastName;
    var gender = request.body.gender;
    var dateOfBirth = request.body.dateOfBirth;
    var profession = request.body.profession;
    var nationality = request.body.nationality;
    var passportNo = request.body.passportNo;
    var passportExpiryDate = request.body.passportExpiryDate;
    var passportCreatedDate = request.body.passportCreatedDate;
    var issueCountry = request.body.issueCountry;
    var issueGov = request.body.issueGov;

    mongoose.model('Profile').count({passportno:passportNo,nationality:nationality},function(error, count){
        if(error){
            console.log(error);
            var errorResponse = { code:500,status:'Error Occured while Processing Request.'};
            response.end(JSON.stringify(errorResponse));
        }else{
            console.log('The Count of Users with the same Passport :'+count);
            if(count > 0){
                var userAlreadyExistResponse = { code:201,status:'User Already Exists.Please try with different PassportNo/Nationality Combination.'};
                response.end(JSON.stringify(userAlreadyExistResponse));
            }else{
                mongoose.model('Profile').create({
                    firstname : firstName,
                    middlename : middleName,
                    lastname : lastName,
                    gender : gender,
                    dateofbirth : dateOfBirth,
                    profession : profession,
                    passportno : passportNo,
                    nationality : nationality,
                    passportexpirydate : passportExpiryDate,
                    passportcreateddate : passportCreatedDate,
                    issuecountry : issueCountry,
                    issuegov : issueGov
                }, function (err, profile) {
                      if (err) {
                        response.end(JSON.stringify(errorResponse));
                        console.log('Error Occured '+err);
                      }
                      else {
                        var userCreatedResponse = { code:200,status:'Profile Created Sucessfully.'};
                        console.log('Created Profile'+profile);
                        response.send(userCreatedResponse);
                      }
                   });

            }

        }
    }
    );



}
);


router.post("/update/profile", function(request, response) {
        var firstName = request.body.firstName;
        var middleName =  request.body.middleName;
        var lastName  = request.body.lastName;
        var gender = request.body.gender;
        var dateOfBirth = request.body.dateOfBirth;
        var profession = request.body.profession;
        var nationality = request.body.nationality;
        var passportno = request.body.passportno;
        var passportExpiryDate = request.body.passportExpiryDate;
        var passportCreatedDate = request.body.passportCreatedDate;
        var issueCountry = request.body.issueCountry;
        var issueGov = request.body.issueGov;
        console.log('document ID :'+ request.body.id);

        var attributesToUpdate =  { 
            firstname :  firstName,
            middlename : middleName,
            lastname : lastName,
            gender : gender,
            dateofbirth : dateOfBirth,
            profession : profession,
            nationality : nationality,
            passportexpirydate : passportExpiryDate,
            passportcreateddate : passportCreatedDate,
            issuecountry : issueCountry,
            issuegov : issueGov
         };
        console.log('attributesToUpdate :'+ attributesToUpdate);

    
        mongoose.model('Profile').findOneAndUpdate(
            {_id : new mongoose.Types.ObjectId(request.body.id)} ,{$set:attributesToUpdate}, {upsert: true, 'new': true}, function (err, user) {
              if (err) {
                console.log('Error Occured '+err);
                response.end("There was a problem adding the information to the database.");
              } else {
                if(user){
                    console.log('The User :'+user);
                    response.end("Data updated successfully.........");
                }
                response.end("No records Found for update.....");
              }
           });
    
    }
);



router.post("/delete/profile", function(request, response) {
    console.log('document ID :'+ request.body.id);
    mongoose.model('Profile').findOneAndRemove(
        {_id : new mongoose.Types.ObjectId(request.body.id)} , function (err, user) {
          if (err) {
            console.log('Error Occured '+err);
            response.end("There was a problem adding the information to the database.");
          } else {
            if(user){
                console.log(user);
                response.end("Data deleted successfully.........");
            }
            response.end("No records Found for update.....");
          }
       });

    }
);
    
router.post("/find/profile", function(request, response) {
    
        var nationality = request.body.nationality;
        var passportno = request.body.passportNo;
        console.log('nationality :'+ nationality);
        console.log('passportno :'+ passportno);
    
        mongoose.model('Profile').findOne({
            nationality : nationality,passportno:passportno
        }, function (err, profile) {
                if (err) {
                    console.log('Error Occured '+err);
                    response.end("There was a problem adding the information to the database.");
                }
                else {
                  console.log('The Profile Found : '+ profile);
                  if(profile){              
                    mongoose.model('Travel').findOne({
                        nationality : nationality,passportno:passportno
                    }                                                      
                    ).sort('-traveldate').exec(function(err, travel) { 
                        if(err){
                            console.log('Error Occured while fetching travel details.. '+ error);
                            var responseObjWithTravelError = { 
                                'code' : 500,
                                'status' : 'Error Occured while fechting Travel Details.'
                              }
                              response.status(500).json(responseObjWithTravelError);    
                              response.end;                            
                        }else{
                            if(travel){
                                console.log('The Travel :  '+ travel);
                                
                                mongoose.model('Permit').findOne({
                                    permitstatus : 'Used'
                                }, function (permiterr, permit) {
                                    if(permiterr){
                                        console.log('Error Occured while fetching permit details.. '+ permiterr);
                                        var responseObjWithPermitError = { 
                                            'code' : 500,
                                            'status' : 'Error Occured while fechting Permit Details.'
                                          }
                                          response.status(500).json(responseObjWithPermitError);    
                                          response.end;    

                                    }else{
                                        if(permit){
                                            var responseObjWithTravelAndPermit = { 
                                                'code' : 200,
                                                'status' : 'Permit Found',
                                                'profile' : profile,
                                                'travel' : travel,
                                                'permit' : permit
                                              }
                                              response.status(200);   
                                              response.end(JSON.stringify(responseObjWithTravelAndPermit));  

                                        }else{

                                            var responseObjWithTravel = { 
                                                'code' : 200,
                                                'status' : 'Permit Not Found',
                                                'profile' : profile,
                                                'travel' : travel
                                              }
                                              response.status(200);   
                                              response.end(JSON.stringify(responseObjWithTravel));  

                                        }
                                    }

                                }
                                );                          
                            }else{
                                var responseObjWithoutTravel = { 
                                    'code' : 200,
                                    'status' : 'Profile Found',
                                    'profile' : profile
                                  }
                                  response.status(200).json(responseObjWithoutTravel);       
                            }
                            response.end;
                        }   
                     });
                  }else{
                  var responseObjWithNoProfile = { 
                    'code' : 404,
                    'status' : 'No Profile Found'
                  }
                  response.status(200).json(responseObjWithNoProfile);
                  response.end;
                  }
                }
            });
    
    }
);
    



module.exports = router;