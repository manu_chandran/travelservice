var express = require('express');
var router = express.Router();
var db = require('../mongodb');
var travel = require('../model/travel.model');
var permit = require('../model/permit.model');
var mongoose = require('mongoose');
mongoose.set('debug', true);



router.post("/mark/exit", function(request, response) {

    var passportNo = request.body.passportNo;
    var nationality = request.body.nationality; 
    var toCountry = request.body.toCountry; 
    var flightNo = request.body.flightNo; 
    var travelDate = new Date();
    var carierType = 'Airport'; 
    var visaType = 'On Arrival';
    var permitStatus =  'Used';
    var travelType = 'Exit';


    console.log('passportNo :'+ passportNo);
    console.log('nationality :'+ nationality);

    mongoose.model('Permit').count({passportno:passportNo,nationality:nationality,permitstatus:permitStatus},function(error, count){
        if(error){
            console.log(error);
            var responseObjWithPermitCountError = { 
                'code' : 500,
                'status' : 'Error Occured while fetching Permit detais.'
          } 
          response.status(500).json(responseObjWithPermitCountError);    
          response.end;   
        }else{
            console.log('The Count of Permits having Used Status with the passportNo :'+ passportNo + ' and nationality :'+nationality+' : '+count);
            if(count == 0){
                console.log('Last Travel was Exit.......');
                var responseObjWithLastTravelExit = { 
                    'code' : 205,
                    'status' : 'Last Travel was Exit'
              } 
              response.status(200).json(responseObjWithLastTravelExit);    
              response.end;    
            }else{
                var attributesToUpdate =  { 
                    permitstatus : 'Expired'
                };
                mongoose.model('Permit').findOneAndUpdate(
                    {passportno:passportNo,nationality:nationality,permitstatus:permitStatus} ,{$set:attributesToUpdate}, {upsert: true, 'new': true}, function (err, permit) {
                      if (err) {
                        console.log('Error Occured '+err);
                        console.log(error);
                        var responseObjWithPermitUpdateError = { 
                            'code' : 500,
                            'status' : 'Error Occured while updating Permit detais.'
                      } 
                      response.status(500).json(responseObjWithPermitUpdateError);    
                      response.end;   
                      } else {
                        if(permit){
                            console.log('The permit :'+permit);
                            console.log('Going to mark travel for Exit..............');
                            mongoose.model('Travel').create({
                                passportno : passportNo,
                                nationality : nationality,
                                traveltype : travelType,
                                traveldate : travelDate,
                                tocountry : toCountry,
                                cariertype : carierType,
                                flightno : flightNo,
                                permitno : permit.permitno
                            }, function (err, travel) {
                                  if (err) {
                                    console.log('Error Occured '+err);
                                    console.log(error);
                                    var responseObjWithMarkExitError = { 
                                        'code' : 500,
                                        'status' : 'Error Occured while marking exit travel detais.'
                                  } 
                                  response.status(500).json(responseObjWithMarkExitError);    
                                  response.end; 
                                  }
                                  else {
                                    console.log('Travel Mark'+travel);
                                    console.log('Exit Travel Marked Successfully');
                                    var responseObjWithTravelMarked = { 
                                        'code' : 200,
                                        'status' : 'Trave Marked Successfully...',
                                        'permitnumber' : permit.permitno
                                    } 
                                    response.status(200).json(responseObjWithTravelMarked);    
                                    response.end;
                                  }
                               });

                        }else{
                            console.log('No Active Permit Found to Mark Travel');
                            var responseObjWithNoActivePermitFound = { 
                                'code' : 206,
                                'status' : 'No Active Permit Found'
                            } 
                            response.status(200).json(responseObjWithNoActivePermitFound);    
                            response.end;
                        }
                      }
                   });

            }

        }
    }
    );



}
);


module.exports = router;