var express = require("express");
var http = require("http");
var bodyParser = require('body-parser')
var morgan = require('morgan');
var app = express();
var config = require('./config');
var loginRoutes = require('./routes/login.route');
var entryRoutes = require('./routes/entry.route');
var exitRoutes = require('./routes/exit.route');
var profileRoutes = require('./routes/profile.route');
var travelRoutes = require('./routes/travel.history.route');
var testRoutes = require('./routes/test.route');
var jwt = require('jsonwebtoken'); 




app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

// use morgan to log requests to the console
app.use(morgan('dev'));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,token");
  next();
});

app.use('/rest', loginRoutes);

// route middleware to verify a token
app.use(function(req, res, next) {

   if(req.method != 'OPTIONS'){
    var token = req.headers['token'];
    if (token) {
      jwt.verify(token, config.secret, function(err, decoded) {      
        if (err) {
          return res.status(402).json({ success: false, message: 'Failed to authenticate token.' });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    }else {
      return res.status(403).send({ 
      success: false, 
      message: 'No token provided.' 
    });
    } 
}else{
  next();
}

});

 



app.use('/rest', profileRoutes);
app.use('/rest', entryRoutes);
app.use('/rest', exitRoutes);
app.use('/rest', travelRoutes);

//Test Route
app.use('/test', testRoutes);

app.use(function(request, response) {
    response.statusCode = 404;
    response.end("404!");
    });


http.createServer(app).listen(3000);


console.log('Server startup complete, listening @ port 3000.......');