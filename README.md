# TravelService

This is a MEAN Stack project demonstrating the prototype functionality of a Immigration application.The whole applicaton comprises 
of two modules, the client and the server and this module contains the server side functionality.


## Getting Started

Please clone the project using the command - git clone https://manu_chandran@bitbucket.org/manu_chandran/travelservice.git

To execute the related Client side functionality, clone the dependent project - git clone https://manu_chandran@bitbucket.org/manu_chandran/travelclient.git


### Prerequisites

Node JS should be installed for executing this application.Apart from Node, mongodb is also required.The application expect mongodb to be up and running
at localhost:27017. For any configuration tweaking, please modify the config file - config.js  



### Installing

1. Clone the project to the local workspace

2. Navigate to the project base folder and execute the command - npm install.

3. Once all the required modules are installed, execute the command - 'npm start'
						
4. Application will be up and running and can be accesed through the url - http://localhost:3000/	
 
5. To Configure the client side application,please refer the readme file for the client project - https://manu_chandran@bitbucket.org/manu_chandran/travelclient.git


## Built With

* [Express JS](https://angular.io/) - Web Application framework
* [Node JS](https://nodejs.org/en/) -JS Runtime Environment
* [MongoDB](https://docs.mongodb.com/) - Datastore


## Author

* **Manu Chandran Shyamala** - (https://bitbucket.org/%7Ba8393a87-bbe7-4fbe-8ad8-1abece085926%7D/)


## License
