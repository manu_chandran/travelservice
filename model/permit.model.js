var mongoose = require('mongoose');  
var permitSchema = new mongoose.Schema({ 
  permitno:String,
  visatype:String,
  permitstatus:String,
  createddate:Date,
  expirydate:Date,   
  establishment: String,
  passportno:String,
  nationality:String
});
mongoose.model('Permit', permitSchema);