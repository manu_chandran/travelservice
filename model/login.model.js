var mongoose = require('mongoose');  
var userSchema = new mongoose.Schema({ 
  firstname:String,
  middlename:String,
  lastname:String,
  createddate:Date,
  modifieddate:Date,   
  username: String,
  password: String,
  is_active: Boolean
});
mongoose.model('User', userSchema);