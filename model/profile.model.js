var mongoose = require('mongoose');  
var profileSchema = new mongoose.Schema({ 
  firstname:String,
  middlename:String,
  lastname:String,
  gender:String,
  dateofbirth:Date,
  profession:String,
  nationality:String,
  passportno:String,
  passportexpirydate:Date,
  passportcreateddate:Date,
  issuecountry:String,
  issuegov:String
});
mongoose.model('Profile', profileSchema);