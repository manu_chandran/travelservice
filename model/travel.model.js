var mongoose = require('mongoose');  
var travelSchema = new mongoose.Schema({ 
  passportno:String,
  nationality:String,
  traveltype:String,
  traveldate:Date,
  fromcountry:String,
  tocountry:String,
  cariertype:String,
  flightno:String,   
  permitno: String,
  remarks: String
});
mongoose.model('Travel', travelSchema);