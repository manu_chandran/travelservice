var mongoose = require('mongoose');
var config = require('./config');
mongoose.connect(config.database,{ useMongoClient: true },function(err){
    if(err){
        console.log('Error Occured while connecting to Mongoose : '+ err);
        throw err;
    }   
});